<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;

Route::get('/', [FrontEndController::class, 'index'])->name('blogs.index');
Route::get('/categories/{category}', [FrontEndController::class, 'category'])->name('blogs.category');
Route::get('/tags/{tag}', [FrontEndController::class, 'tag'])->name('blogs.tag');

Route::get('/blogs/{post}', [FrontEndController::class, 'show'])->name('blogs.show');

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('admin-panel.dashboard');
    })->name('dashboard');
    Route::resource('tags', TagsController::class)->except('show');
    Route::resource('categories', CategoriesController::class)->except('show');
    Route::resource('posts', PostsController::class);
    Route::put('posts/{post}/publish-now', [PostsController::class, 'publish'])->name('posts.publish');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
