<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="index.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">Masters</div>
                <a class="nav-link" href="index.html">
                    <div class="sb-nav-link-icon">
                        <i class="fa-solid fa-tag"></i>
                    </div>
                    Tags
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <a class="nav-link" href="index.html">
                    <div class="sb-nav-link-icon">
                        <i class="fa-solid fa-gears"></i>
                    </div>
                    Categories
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="sb-sidenav-menu-heading">Blogs</div>
                <a class="nav-link" href="index.html">
                    <div class="sb-nav-link-icon">
                        <i class="fa-solid fa-rss"></i>
                    </div>
                    Posts
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <a class="nav-link" href="index.html">
                    <div class="sb-nav-link-icon">
                        <i class="fa-solid fa-trash"></i>
                    </div>
                    Trash Posts
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            Start Bootstrap
        </div>
    </nav>
</div>
