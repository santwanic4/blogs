@extends('admin-panel.layouts.admin')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.8/dist/trix.css">
@endsection

@section('admin-panel-content')
    <div class="container-fluid my-4 px-4">
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between">
                <h2 class="mt-4 d-inline-block">Edit Post</h2>
            </div>

            <div class="card-body">
                <div class="datatable-wrapper datatable-loading no-footer sortable searchable fixed-columns">
                    <form action={{ route('posts.update', $post)}} method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="name" class="form-label">Title</label>
                            <input type="text" name="title"  class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Post's title" value="{{old('title', $post->title)}}">
                            <div id="nameHelp" class="form-text text-danger">
                                @error('title')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="excerpt" class="form-label">Excerpt</label>
                            <textarea type="text" name="excerpt"  class="form-control @error('excerpt') is-invalid @enderror" id="excerpt">{{old('excerpt', $post->excerpt)}}</textarea>
                            <div id="nameHelp" class="form-text text-danger">
                                @error('excerpt')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="category_id" class="form-label">Category</label>
                            <select name="category_id"  class="select2 form-control" id="category_id" value="{{old('category')}}">
                                <option value="select..." selected disabled>Select...</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->id === old('category_id', $post->category_id) ? 'selected' : ''}}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <div id="nameHelp" class="form-text text-danger">
                                @error('category_id')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="body" class="form-label">Body</label>
                            <input type="hidden" name="body"  class="form-control @error('body') is-invalid @enderror" id="body" value="{{old('body', $post->body)}}">
                            <trix-editor input="body"></trix-editor>
                            <div id="nameHelp" class="form-text text-danger">
                                @error('body')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="image" class="form-label">Image</label>
                            <input type="file" name="image"  class="form-control" id="image" accept=".jpg,.gif,.png">
                            <div id="nameHelp" class="form-text text-danger">
                                @error('image')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="published_at" class="form-label">Published At</label>
                            <input type="datetime-local" name="published_at"  class="form-control" id="published_at">
                            <div id="nameHelp" class="form-text text-danger">
                                Keep it blank to save it as draft
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="tags" class="form-label">Tags</label>
                            <select name="tags[]" class="form-control select2" id="tags" multiple="multiple">
                                @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}" {{ in_array($tag->id, $post->tags->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                @endforeach
                            </select>
                            <div id="nameHelp" class="form-text text-danger">
                                @error('tags')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary mt-3">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.8/dist/trix.umd.min.js"></script>
@endsection
