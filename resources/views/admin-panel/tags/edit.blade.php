@extends('admin-panel.layouts.admin')

@section('admin-panel-content')
    <div class="container-fluid my-4 px-4">
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between">
                <h2 class="mt-4 d-inline-block">Add Tag</h2>
            </div>

            <div class="card-body">
                <div class="datatable-wrapper datatable-loading no-footer sortable searchable fixed-columns">
                    <div class="mb-3">
                        <form action={{ route('tags.update', $tag)}} method="POST">
                            @csrf
                            @method('PUT')
                            <label for="name" class="form-label">Name</label>
                            <input type="text" name="name"  class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Tag's name" value="{{old('name', $tag->name)}}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror

                            <button type="submit" class="btn btn-primary mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
