<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tags = Tag::latest()->paginate(5);
        return view('admin-panel.tags.index', compact('tags'));
    }

    public function create()
    {
        return view('admin-panel.tags.create');
    }

    public function store(CreateTagRequest $request)
    {
        $tag = Tag::create(['name' => $request->get('name')]);
        if(!$tag) {
            return redirect(route('tags.index'))->with('error', 'Issue while creating tag!');
        } else {
            return redirect(route('tags.index'))->with('success', 'Tag created successfully!');
        }
    }

    public function edit(Tag $tag)
    {
        return view('admin-panel.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->name = $request->name;
        if($tag->isClean()) {
            return redirect(route('tags.index'))->with('error', 'You didn\'t change any field!');
        }
        $tag->save();
        return redirect(route('tags.index'))->with('success', 'Tag updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect(route('tags.index'))->with('success', 'Tag Deleted Successfully');
    }
}
