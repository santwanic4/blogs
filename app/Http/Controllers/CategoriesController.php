<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->paginate(5);
        return view('admin-panel.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin-panel.categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = Category::create(['name' => $request->get('name')]);
        if(!$category) {
            return redirect(route('categories.index'))->with('error', 'Issue while creating category!');
        } else {
            return redirect(route('categories.index'))->with('success', 'Category created successfully!');
        }
    }

    public function edit(Category $category)
    {
        return view('admin-panel.categories.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        if($category->isClean()) {
            return redirect(route('categories.index'))->with('error', 'You didn\'t change any field!');
        }
        $category->save();
        return redirect(route('categories.index'))->with('success', 'Category updated successfully');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('categories.index'))->with('success', 'Category deleted successfully');
    }
}
