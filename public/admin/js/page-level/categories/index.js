document.addEventListener('DOMContentLoaded', init)

function init(){
    const deleteBtns = document.querySelectorAll('.delete-category');
    deleteBtns.forEach((btn) => btn.addEventListener('click', deleteCategory));
}

function deleteCategory() {
    const route = this.dataset.deleteRoute;
    const deleteForm = document.querySelector('#deleteForm');
    deleteForm.setAttribute('action', route);
    const deleteModal = new bootstrap.Modal('#deleteModal');
    deleteModal.show();
}

